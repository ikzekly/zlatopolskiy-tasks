package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.25. Заполнить двумерный массив так, как представлено на рис. 12.3.
 *
 * (tests can be skipped. don't use any arithmetic calculations based on i,j’s values to detect array’s [i,j] value,
 * note that all is about direction of array traversal)
 * */

import java.util.Scanner;

public class TaskCh12N025 {
    public static void main(String[] args) {
        char symbolOfMatrix = inputChar();
        switch (symbolOfMatrix) {
            case 'a':
                int[][] a = matrixA(12, 10);
                printMatrix(a);
                break;
            case 'b':
                int[][] b = matrixB(12, 10);
                printMatrix(b);
                break;
            case 'c':
                int[][] c = matrixC(12, 10);
                printMatrix(c);
                break;
            case 'd':
                int[][] d = matrixD(12, 10);
                printMatrix(d);
                break;
            case 'e':
                int[][] e = matrixE(12, 10);
                printMatrix(e);
                break;
            case 'f':
                int[][] f = matrixF(12, 10);
                printMatrix(f);
                break;
            case 'g':
                int[][] g = matrixG(12, 10);
                printMatrix(g);
                break;
            case 'h':
                int[][] h = matrixH(12, 10);
                printMatrix(h);
                break;
            case 'i':
                int[][] i = matrixI(12, 10);
                printMatrix(i);
                break;
            case 'j':
                int[][] j = matrixJ(12, 10);
                printMatrix(j);
                break;
            case 'k':
                int[][] k = matrixK(12, 10);
                printMatrix(k);
                break;
            case 'l':
                int[][] l = matrixL(12, 10);
                printMatrix(l);
                break;
            case 'm':
                int[][] m = matrixM(12, 10);
                printMatrix(m);
                break;
            case 'n':
                int[][] n = matrixN(12, 10);
                printMatrix(n);
                break;
            case 'o':
                int[][] o = matrixO(12, 10);
                printMatrix(o);
                break;
            case 'p':
                int[][] p = matrixP(12, 10);
                printMatrix(p);
                break;
        }
    }

    private static char inputChar() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter character of matrix: ");
        String str = scNumber.next();
        char result = str.charAt(0);

        return result;
    }

    public static int[][] matrixA(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];

        for (int i = 0; i < lengthArr * widthArr; i++) {
            arr[i / widthArr][i % widthArr] = i + 1;
        }
        return arr;
    }

    public static int[][] matrixB(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < widthArr; i++) {
            for (int j = 0; j < lengthArr; j++) {
                num++;
                arr[j][i] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixC(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < lengthArr; i++) {
            for (int j = widthArr - 1; j >= 0; j--) {
                num++;
                arr[i][j] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixD(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < widthArr; i++) {
            for (int j = lengthArr - 1; j >= 0; j--) {
                num++;
                arr[j][i] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixE(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < lengthArr; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < widthArr; j++) {
                    num++;
                    arr[i][j] = num;
                }
            } else {
                for (int j = widthArr - 1; j >= 0; j--) {
                    num++;
                    arr[i][j] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixF(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < widthArr; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < lengthArr; j++) {
                    num++;
                    arr[j][i] = num;
                }
            } else {
                for (int j = lengthArr - 1; j >= 0; j--) {
                    num++;
                    arr[j][i] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixG(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = lengthArr - 1; i >= 0; i--) {
            for (int j = 0; j < widthArr; j++) {
                num++;
                arr[i][j] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixH(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = widthArr - 1; i >= 0; i--) {
            for (int j = 0; j < lengthArr; j++) {
                num++;
                arr[j][i] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixI(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = lengthArr - 1; i >= 0; i--) {
            for (int j = widthArr - 1; j >= 0; j--) {
                num++;
                arr[i][j] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixJ(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = widthArr - 1; i >= 0; i--) {
            for (int j = lengthArr - 1; j >= 0; j--) {
                num++;
                arr[j][i] = num;
            }
        }
        return arr;
    }

    public static int[][] matrixK(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = lengthArr - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int j = 0; j < widthArr; j++) {
                    num++;
                    arr[i][j] = num;
                }
            } else {
                for (int j = widthArr - 1; j >= 0; j--) {
                    num++;
                    arr[i][j] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixL(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < lengthArr; i++) {
            if (i % 2 == 0) {
                for (int j = widthArr - 1; j >= 0; j--) {
                    num++;
                    arr[i][j] = num;
                }
            } else {
                for (int j = 0; j < widthArr; j++) {
                    num++;
                    arr[i][j] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixM(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = widthArr - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int j = 0; j < lengthArr; j++) {
                    num++;
                    arr[j][i] = num;
                }
            } else {
                for (int j = lengthArr - 1; j >= 0; j--) {
                    num++;
                    arr[j][i] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixN(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = 0; i < widthArr; i++) {
            if (i % 2 == 1) {
                for (int j = 0; j < lengthArr; j++) {
                    num++;
                    arr[j][i] = num;
                }
            } else {
                for (int j = lengthArr - 1; j >= 0; j--) {
                    num++;
                    arr[j][i] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixO(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = lengthArr - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < widthArr; j++) {
                    num++;
                    arr[i][j] = num;
                }
            } else {
                for (int j = widthArr - 1; j >= 0; j--) {
                    num++;
                    arr[i][j] = num;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixP(int lengthArr, int widthArr) {
        int[][] arr = new int[lengthArr][widthArr];
        int num = 0;
        for (int i = widthArr - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j < lengthArr; j++) {
                    num++;
                    arr[j][i] = num;
                }
            } else {
                for (int j =lengthArr - 1; j >= 0; j--) {
                    num++;
                    arr[j][i] = num;
                }
            }
        }
        return arr;
    }


    private static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
