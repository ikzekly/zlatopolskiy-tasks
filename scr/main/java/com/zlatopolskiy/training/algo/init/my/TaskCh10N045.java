package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.45. Даны первый член и разность арифметической прогрессии. Написать рекур-
 * сивную функцию для нахождения:
 * а) n-го члена прогрессии;
 * б) суммы n первых членов прогрессии.
 * */

public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.print("Enter the first element: ");
        int firstElement = TaskCh01N003.inputNum();
        System.out.print("Enter the difference of arithmetical progression: ");
        int diff = TaskCh01N003.inputNum();
        System.out.print("Enter the position of seeking number: ");
        int poss = TaskCh01N003.inputNum();

        System.out.println("The n-th term of the progression = " + getArithmeticProgression(firstElement, diff, poss));
        System.out.println("");

        System.out.print("Enter the position of last element for summ: ");
        int numberOfLastElement = TaskCh01N003.inputNum();
        String resultText = "The sum of n first terms of an arithmetic progression = ";
        System.out.println(resultText + getSummOfArithmeticProgression(firstElement, diff, numberOfLastElement));
    }

    public static int getArithmeticProgression(int firstNumber, int difference, int numberOfMember) {
        if (numberOfMember > 1)
            return getArithmeticProgression(firstNumber + difference, difference, numberOfMember - 1);
        else
            return firstNumber;
    }

    public static int getSummOfArithmeticProgression(int firstNumber, int difference, int numberOfLastElement) {
        if (numberOfLastElement > 1) {
            int getElement = firstNumber + (numberOfLastElement - 1) * difference;
            return getElement + getSummOfArithmeticProgression(firstNumber, difference, numberOfLastElement - 1);
        } else
            return firstNumber;
    }
}
