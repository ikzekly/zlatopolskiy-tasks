package main.java.com.zlatopolskiy.training.algo.init.my;
/**
 * 1.3. Составить программу вывода на экран числа, вводимого с клавиатуры. Выво-
 * димому числу должно предшествовать сообщение "Вы ввели число".
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        try {
            System.out.println("Input the number: ");
            int num = inputNum();
            outputNum(num);
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            e.printStackTrace();
        }
    }

    public static int inputNum() throws InputMismatchException {
        Scanner scNumber = new Scanner(System.in);
        int num = scNumber.nextInt();
        checkIntegerValue(num);
        return num;
    }

    private static boolean checkIntegerValue(int num) throws InputMismatchException {
        if (num > Integer.MIN_VALUE && num < Integer.MAX_VALUE) {
            return true;
        } else throw new InputMismatchException();
    }

    private static void outputNum(int num) {
        System.out.println("You entered number is " + num);
    }
}
