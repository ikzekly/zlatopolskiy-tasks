package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 4.15. Известны год и номер месяца рождения человека, а также год и номер месяца
 * сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число
 * полных лет). В случае совпадения указанных номеров месяцев считать, что
 * прошел полный год.
 * */

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        System.out.println("First input the month and year of BIRTH date, then the year and month of the CURRENT date.\n");
        CustomDate birthdayDate = inputDate();
        CustomDate todayDate = inputDate();

        int result = findAge(birthdayDate.getMonth(), birthdayDate.getYear(), todayDate.getMonth(), todayDate.getYear());
        System.out.println("Age is " + result);

    }

    private static CustomDate inputDate() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the month: ");
        int inputMonth = scNumber.nextInt();

        System.out.println("Enter the year: ");
        int inputYear = scNumber.nextInt();

        CustomDate inputDate = new CustomDate(inputMonth, inputYear);

        return inputDate;
    }



    public static int findAge(int bdMonth, int bdYear, int todayMonth, int todayYear) {

        int diffMonth = todayMonth - bdMonth;
        int diffYear = todayYear - bdYear;

        int resultAge;
        if (diffMonth < 0) {
            resultAge = diffYear - 1;
        } else
            resultAge = diffYear;

        return resultAge;
    }

}

class CustomDate {
    private int month;
    private int year;

    public CustomDate(int month, int year) {
        this.month = month;
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
