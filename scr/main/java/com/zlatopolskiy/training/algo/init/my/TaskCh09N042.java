package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.42. Составить программу, которая печатает заданное слово, начиная с последней
 * буквы.
 *
 * (use StringBuilder)
 * */

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.println(reverseStr(inputStr()));
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the string: ");
        String str = scNumber.next();

        return str;
    }

    public static String reverseStr(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();

        return sb.toString();
    }
}
