package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 6.87. Составить программу, которая ведет учет очков, набранных каждой командой
 * при игре в баскетбол. Количество очков, полученных командами в ходе игры,
 * может быть равно 1, 2 или 3. После любого изменения счет выводить на
 * экран. После окончания игры выдать итоговое сообщение и указать номер
 * команды-победительницы. Окончание игры условно моделировать вводом
 * количества очков, равного нулю.
 * */

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        sumPointsAndPrintResult();
    }

    private static int inputTeamNumber() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the number of team (1 or 2): ");
        int inNumber = scNumber.nextInt();

        return inNumber;
    }

    private static int inputPoints() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the number of points(1, 2, 3): ");
        int inNumber = scNumber.nextInt();

        return inNumber;
    }

    private static String printScore(int oneTeamPoints, int twoTeamPoints) {

        if (oneTeamPoints > twoTeamPoints) {
            System.out.println("First team ahead!");
        } else if (oneTeamPoints < twoTeamPoints) {
            System.out.println("Second team ahead!");
        } else {
            System.out.println("Currently a draw.");
        }

        return "Current score: " + oneTeamPoints + " : " + twoTeamPoints;
    }

    private static String printWhoWon(int oneTeamPoints, int twoTeamPoints) {

        System.out.println("");
        if (oneTeamPoints > twoTeamPoints) {
            System.out.println("The first team has won!");
        } else if (oneTeamPoints < twoTeamPoints) {
            System.out.println("The second team has won!");
        } else System.out.println("No one won.");

        return "Current score: " + twoTeamPoints + ":" + oneTeamPoints;

    }

    private static int validationPoints(int points) {
        if (points >= 0 && points <= 3) {
            return points;
        } else throw new RuntimeException("Incorrect value for score!(Only 1, 2, 3 or 0 for exit)");
    }

    private static int validationTeam(int points) {
        if (points >= 1 && points <= 2) {
            return points;
        } else throw new RuntimeException("Incorrect value for team number!(Only 1 or 2)");
    }

    public static void sumPointsAndPrintResult() {

        int points = -1;
        int team = 0;
        int oneTeamPoints = 0;
        int twoTeamPoints = 0;

        while (points != 0) {
            points = validationPoints(inputPoints());
            team = validationTeam(inputTeamNumber());

            if (team == 1) {
                oneTeamPoints += points;
                System.out.println(printScore(oneTeamPoints, twoTeamPoints));
            } else if (team == 2) {
                twoTeamPoints += points;
                System.out.println(printScore(oneTeamPoints, twoTeamPoints));
            }
        }
        System.out.println(printWhoWon(oneTeamPoints, twoTeamPoints));
    }
}




