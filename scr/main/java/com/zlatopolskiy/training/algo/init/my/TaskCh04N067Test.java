package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        testCheckDayOfWeek();
    }

    private static void testCheckDayOfWeek() {
        Assert.assertEquals("TaskCh04N067", "Workday", TaskCh04N067.checkDayOfWeek(5));
        Assert.assertEquals("TaskCh04N067", "Weekend", TaskCh04N067.checkDayOfWeek(7));
    }
}
