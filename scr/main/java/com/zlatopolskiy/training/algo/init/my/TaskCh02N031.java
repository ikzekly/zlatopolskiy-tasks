package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 2.31. В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному
 * при этом двузначному числу справа приписали вторую цифру числа x, то по-
 * лучилось число n. По заданному n найти число x (значение n вводится с кла-
 * виатуры, 100 ≤ n ≤ 999).
 * */


public class TaskCh02N031 {
    public static void main(String[] args) {
        try {
            System.out.println("Enter three-digit number: ");
            int num = TaskCh01N003.inputNum();
            boolean threeDigitNum = TaskCh02N013.checkTheThreeDigitNumber(num);
            if (threeDigitNum) {
                int result = changeInTheNumber(num);
                outputNum(result);
            }
        }catch (InputMismatchException e){
            System.out.println("Input error!");
            e.printStackTrace();
        }
    }

    public static int changeInTheNumber(Integer inputNumber) {

        int firstNum = inputNumber % 10;
        inputNumber /= 10;

        int secondNum = inputNumber % 10;
        inputNumber /= 10;

        int thirdNum = inputNumber % 10;

        int resultInvert = thirdNum * 100 + firstNum * 10 + secondNum;

        return resultInvert;
    }

    private static void outputNum(int value) {
        System.out.println(value);
    }
}
