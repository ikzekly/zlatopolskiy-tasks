package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 4.36. Работа светофора для пешеходов запрограммирована следующим образом: в
 * начале каждого часа в течение трех минут горит зеленый сигнал, затем в те-
 * чение двух минут — красный, в течение трех минут — опять зеленый и т. д.
 * Дано вещественное число t, означающее время в минутах, прошедшее с нача-
 * ла очередного часа. Определить, сигнал какого цвета горит для пешеходов в
 * этот момент.
 */

public class TaskCh04N036 {
    public static void main(String[] args) {
        try {
            int time = TaskCh01N003.inputNum();
            String colorLight = checkColorLight(time);
            System.out.println(colorLight);
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            e.printStackTrace();
        }
    }


    public static String checkColorLight(int time) {
        time %= 5;

        String result;
        if (time >= 3) {
            result = "red";
        } else
            result = "green";

        return result;
    }
}
