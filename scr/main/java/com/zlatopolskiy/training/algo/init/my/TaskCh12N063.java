package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.63. В двумерном массиве хранится информация о количестве учеников в том
 * или ином классе каждой параллели школы с первой по одиннадцатую (в пер-
 * вой строке — информация о количестве учеников в первых классах, во вто-
 * рой — о вторых и т. д.). В каждой параллели имеются 4 класса. Определить
 * среднее количество учеников в классах каждой параллели.
 * */

public class TaskCh12N063 {
    public static void main(String[] args) {

        int[][] arr = inputMatrix(11, 4);

        for (int i = 0; i < arr.length; i++) {
            int res = getAverageVal(arr, i);
            System.out.println(res);
        }
    }

    private static int[][] inputMatrix(int lenght, int width) {
        int[][] arr = new int[lenght][width];
        String[] countOfClassArr = {"first", "second", "the third", "fourth", "fifth", "sixth",
                "seventh", "eighth", "ninth", "tenth", "eleventh"};
        Character[] countOfParallel = {'A', 'B', 'C', 'D'};

        for (int i = 0; i < lenght; i++) {
            for (int j = 0; j < width; j++) {
                System.out.println("Enter count of student in " + countOfClassArr[i] + " parallel, "
                        + countOfParallel[j] + " class : ");
                arr[i][j] = TaskCh01N003.inputNum();
            }
        }
        return arr;
    }

    public static int getAverageVal(int arr[][], int str) {

        int sum = 0;
        int countOfColums = 0;
        for (int i = 0; i < arr.length; i++) {
            countOfColums++;
            sum = sum + arr[str][i];
        }
        int res = sum / countOfColums;
        return res;
    }
}
