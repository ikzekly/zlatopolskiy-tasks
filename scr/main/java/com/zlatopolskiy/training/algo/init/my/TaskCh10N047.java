package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.47. Написать рекурсивную функцию для вычисления k-го члена последователь-
 * ности Фибоначчи. Последовательность Фибоначчи f1, f2, ... образуется по
 * закону: f1=1; f2=1; fi=fi-1+fi-2 ( i=3, 4, ...).
 */

public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.println("Enter the number in the Fibonacci number sequence: ");
        int num = TaskCh01N003.inputNum();
        int fibNum = fib(num);
        System.out.println("Fib = " + fibNum);
    }

    public static int fib(int arg) {
        return
                (arg < 2) ? 1 : fib(arg - 2) + fib(arg - 1);
    }
}
