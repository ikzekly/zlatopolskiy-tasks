package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;


public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngleCalculation();
    }

    private static void testAngleCalculation() {
        Assert.assertEquals("TaskCh02N039", 90, TaskCh02N039.angleCalculation(15, 0, 0));
        Assert.assertEquals("TaskCh02N039", 360, TaskCh02N039.angleCalculation(24, 0, 0));
        Assert.assertEquals("TaskCh02N039", 0, TaskCh02N039.angleCalculation(0, 0, 0));
    }
}
