/**
 * 3.29. Записать условие, которое является истинным, когда:
 * а) каждое из чисел X и Y нечетное;
 * б) только одно из чисел X и Y меньше 20;
 * в) хотя бы одно из чисел X и Y равно нулю;
 * г) каждое из чисел X, Y, Z отрицательное;
 * д) только одно из чисел X, Y и Z кратно пяти;
 * е) хотя бы одно из чисел X, Y, Z больше 100.
 *
 * */

/**
* a)
* @return true when x%2==0 & y%2==0;
*
* b)
* @return true when x<20 ^ y<20;
*
* c)
* @return true when x==0 | y==0;
*
* d)
* @return true when x<0 & y<0 & z<0;
*
* e)
* @return true when (x%5==0 ^ y%5==0 ^ z%5==0) & !(x%5==0 & y%5==0 & z%5==0);
*
* f)
* @return true when x>100 | y>100 | z>100;
*
*/

package main.java.com.zlatopolskiy.training.algo.init.my;

public class TaskCh03N029 {
}
