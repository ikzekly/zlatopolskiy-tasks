package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.49. Написать рекурсивную функцию для вычисления индекса максимального
 * элемента массива из n элементов.
 * */

import java.util.Scanner;

public class TaskCh10N049 {
    public static void main(String[] args) {
        String str = inputStr();
        int[] inputArr = strToIntArray(str);
        int startSearch = 0;
        System.out.println(maxIndex(inputArr, startSearch));
    }

    private static String inputStr() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a sequence of numbers separated by a space : ");
        String inputStr = sc.nextLine();

        return inputStr;
    }

    private static int[] strToIntArray(String str) {
        StringBuilder sbStr = new StringBuilder(str);
        String[] arrayStr = sbStr.toString().split("\\s+");

        int[] arrayNum = new int[arrayStr.length];
        for (int i = 0; i < arrayStr.length; i++) {
            arrayNum[i] = Integer.parseInt(arrayStr[i]);
        }
        return arrayNum;
    }

    public static int maxIndex(int[] arr, int start) {
        for (int i = 1; i < arr.length; i++) {
            if (arr[start] < arr[i])
                return maxIndex(arr, i);
        }
        return start;
    }
}