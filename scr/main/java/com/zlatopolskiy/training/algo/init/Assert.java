package main.java.com.zlatopolskiy.training.algo.init;

public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {

        if (expected == actual) {

            System.out.println(testName + " success");

        } else {

            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);

        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {

        if (expected == actual) {

            System.out.println(testName + " success");

        } else {

            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);

        }

    }

    public static void assertEquals(String testName, String expected, String actual) {

        if (expected.equals(actual)) {

            System.out.println(testName + " success");

        } else {

            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);

        }

    }

    public static void assertEquals(String testName, double expected, double actual) {

        if (expected == actual) {

            System.out.println(testName + " success");

        } else {

            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);

        }

    }
}
