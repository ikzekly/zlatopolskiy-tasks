package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testInvertValue();
    }

    private static void testInvertValue() {

        Assert.assertEquals("TaskCh02N013Test", 123, TaskCh02N013.invertValue(321));
    }
}
