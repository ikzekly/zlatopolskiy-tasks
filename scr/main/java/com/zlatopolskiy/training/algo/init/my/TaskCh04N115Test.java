package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        testConversionYear();
    }

    private static void testConversionYear() {
        Assert.assertEquals("TaskCh04N115", "Green Rat", TaskCh04N115.conversionYear(1984));
    }
}
