package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testEvenOrOddNum();
    }

    private static void testEvenOrOddNum() {
        Assert.assertEquals("TaskCh04N033", true, TaskCh04N033.evenOrOddNum(124));
        Assert.assertEquals("TaskCh04N033", false, TaskCh04N033.evenOrOddNum(123));
    }
}
