package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.41. Написать рекурсивную функцию для вычисления факториала натурального
 * числа n.
 * */

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {

        int num = inputNum();
        exceptionGet(num);
        System.out.println("Factorial = " + fact(num));
    }

    private static int inputNum() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the number: ");
        int inputStr = scNumber.nextInt();
        return inputStr;
    }

    public static int fact(int n) {
        if (n == 1)
            return 1;

        return fact(n - 1) * n;
    }

    private static void exceptionGet(int number) {
        if (number > 12)
            throw new RuntimeException("A very large number!");
    }
}
