package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.46. Даны первый член и знаменатель геометрической прогрессии. Написать ре-
 * курсивную функцию:
 * а) нахождения n-го члена прогрессии;
 * б) нахождения суммы n первых членов прогрессии.
 * */

import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        System.out.print("Enter the first element: ");
        int firstElement = TaskCh01N003.inputNum();
        System.out.print("Enter the difference of geometrical progression: ");
        int diff = TaskCh01N003.inputNum();
        System.out.print("Enter the position of seeking number: ");
        int poss = TaskCh01N003.inputNum();

        System.out.println("The n-th term of the progression = " + getGeometricProgression(firstElement, diff, poss) + "\n");

        System.out.print("Enter the position of last element for summ: ");
        int numberOfLastElement = TaskCh01N003.inputNum();
        String resultText = "The sum of n first terms of an arithmetic progression = ";
        System.out.println(resultText + getSummOfGeometricProgression(firstElement, diff, numberOfLastElement));
    }

    public static int getGeometricProgression(int firstNumber, int difference, int numberOfMember) {
        if (numberOfMember > 1)
            return getGeometricProgression(firstNumber * difference, difference, numberOfMember - 1);
        else
            return firstNumber;
    }

    public static int getSummOfGeometricProgression(int firstNumber, int difference, int numberOfLastElement) {
        if (numberOfLastElement > 1) {
            int getElement = getGeometricProgression(firstNumber, difference, numberOfLastElement);
            return getElement + getSummOfGeometricProgression(firstNumber, difference, numberOfLastElement - 1);
        } else
            return firstNumber;
    }
}
