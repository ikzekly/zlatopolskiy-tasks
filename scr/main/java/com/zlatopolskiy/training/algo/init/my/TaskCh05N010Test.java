package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

/**
 * Created by mr.robot on 4/20/2016.
 */
public class TaskCh05N010Test {
    public static void main(String[] args) {
        testFindValueOfRubles();
    }

    private static void testFindValueOfRubles() {
        Assert.assertEquals("TaskCh05N010", 135, TaskCh05N010.findValueOfRubles(67.5, 2));
    }
}
