package main.java.com.zlatopolskiy.training.algo.init.my;

/**10.55.* Написать рекурсивную процедуру перевода натурального числа из десятич-
 *       ной системы счисления в N-ричную. Значение N в основной программе вво-
 *       дится с клавиатуры (2 <= N <= 16).
 */

import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        System.out.print("Enter natural number for convert: ");
        int num = TaskCh01N003.inputNum();
        int numSystem = inputNumSystem();
        convNumFromDecimal(numSystem, num);
    }

    private static int inputNumSystem() {
        Scanner scNumber = new Scanner(System.in);
        System.out.print("Enter the number system: ");
        int inputNumSystem = scNumber.nextInt();
        if (inputNumSystem >= 2 && inputNumSystem <= 16) {
            return inputNumSystem;
        } else throw new RuntimeException("The system numbers must be from '2' to '16'.");
    }

    public static void convNumFromDecimal(int numSyst, int numToConv) {
        Character[] chars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        while (numToConv >= numSyst) {
            convNumFromDecimal(numSyst, numToConv / numSyst);
            numToConv = numToConv % numSyst;
        }
        System.out.print(chars[numToConv]);
    }
}
