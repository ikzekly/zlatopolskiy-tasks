package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.185.*Строка содержит арифметическое выражение, в котором используются
 * круглые скобки, в том числе вложенные. Проверить, правильно ли в нем рас-
 * ставлены скобки.
 * а) Ответом должны служить слова да или нет.
 * б) В случае неправильности расстановки скобок:
 * если имеются лишние правые (закрывающие) скобки, то выдать сооб-
 * щение с указанием позиции первой такой скобки;
 * если имеются лишние левые (открывающие) скобки, то выдать сообще-
 * ние с указанием количества таких скобок.
 * Если скобки расставлены правильно, то сообщить об этом.
 *
 * */

import java.util.Scanner;
import java.util.Stack;

public class TaskCh09N185 {
    public static void main(String[] args) {
        String str = inputStr();
        printResult(checkBraces(str));
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);
        System.out.println("Enter the string: ");
        return scNumber.nextLine();
    }


    public static Boolean checkBraces(String str) {
        if (str.isEmpty())
            return true;

        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < str.length(); i++) {
            Character symbol = str.charAt(i);


            if (symbol == '(') {
                stack.push(symbol);
            } else if (stack.empty()) {
                int place = i + 1;
                System.out.println("Excess right bracket on " + place + " place.");
                return false;
            }

            if (symbol == ')') {
                if (stack.empty())
                    return false;
                else if (stack.peek() == '(')
                    stack.pop();
                else
                    return false;
            }

            int endOfStr = (str.length() - 1) - i;
            if (endOfStr == 0 && stack.size() > 0)
                System.out.println(stack.size() + " extra left brackets.");

        }
        return stack.empty();
    }

    private static void printResult(boolean result) {
        if (result) {
            System.out.println("Yes");
        } else System.out.println("No");
    }
}
