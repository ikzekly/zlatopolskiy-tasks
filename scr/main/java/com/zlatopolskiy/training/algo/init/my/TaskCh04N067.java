package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 4.67. Дано целое число k (1 <= k <= 365). Определить, каким будет k-й день года: вы-
 * ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.
 */

public class TaskCh04N067 {
    public static void main(String[] args) {
        try {
            int day = TaskCh01N003.inputNum();
            if (day <= 365 && day >= 1) {
                String result = checkDayOfWeek(day);
                System.out.println(result);
            } else System.out.println("You must enter a value from 1 to 365.");
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            e.printStackTrace();
        }

    }

    public static String checkDayOfWeek(int day) {
        if (day > 7) {
            day %= 7;
        }

        String result;
        if (day <= 5) {
            result = "Workday";
        } else
            result = "Weekend";

        return result;
    }
}
