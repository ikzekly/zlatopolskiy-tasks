package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 2.43. Даны два целых числа a и b. Если a делится на b или b делится на a, то вывес-
 * ти 1, иначе — любое другое число. Условные операторы и операторы цикла
 * не использовать.
 */

public class TaskCh02N043 {
    public static void main(String[] args) {
        try {
            System.out.println("Enter an integer A: ");
            int a = TaskCh01N003.inputNum();
            System.out.println("Enter an integer B: ");
            int b = TaskCh01N003.inputNum();
            int result = modulo(a, b);
            System.out.println("Result = " + result);
        } catch (InputMismatchException e) {
            System.out.println("Input error!");
            e.printStackTrace();
        }
    }

    public static int modulo(int a, int b) {
        int moduloAB = a % b;
        int moduloBA = b % a;

        return moduloAB * moduloBA + 1;
    }
}
