package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testInvertValue();
    }

    private static void testInvertValue() {

        Assert.assertEquals("Test TaskCh02N031",132, TaskCh02N031.changeInTheNumber(123));
    }
}


