package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.22. Дано слово, состоящее из четного числа букв. Вывести на экран его первую
 * половину, не используя оператор цикла.
 */

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {

        System.out.println(getHalfStr(inputStr()));
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the string: ");
        String str = scNumber.next();

        return str;
    }

    public static String getHalfStr(String str) {

        StringBuilder sb = new StringBuilder(str);
        int middleStr = sb.length() / 2;
        int endStr = sb.length();
        StringBuilder halfStr = sb.delete(middleStr, endStr);
        return halfStr.toString();
    }

}
