package main.java.com.zlatopolskiy.training.algo.init.my;
/**
 * 10.52. Написать рекурсивную процедуру для вывода на экран цифр натурального
 * числа в обратном порядке.
 * */

public class TaskCh10N052 {
    public static void main(String[] args) {

        System.out.println("Enter natural number: ");
        int num = TaskCh01N003.inputNum();
        invertNum(num);
    }

    public static void invertNum(int num) {
        if (num > 0) {
            int last = num % 10;
            System.out.print(last);
            invertNum(num / 10);
        }
    }

}
