package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.50. Написать рекурсивную функцию для вычисления значения так называемой
 * функции Аккермана для неотрицательных чисел n и m.
 * Найти значение функции Аккермана для n 1, m 3.
 * */

public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.print("Enter N: ");
        int n = TaskCh01N003.inputNum();
        System.out.print("Enter M: ");
        int m = TaskCh01N003.inputNum();
        System.out.println(getAckermannFunction(n, m));
    }

    public static int getAckermannFunction(int n, int m) {
        if (n > 0 && m > 0) {
            int second = getAckermannFunction(n, m - 1);
            return getAckermannFunction(n - 1, second);

        } else if (n != 0 && m == 0) {
            return getAckermannFunction(n - 1, 1);

        } else if (n == 0) {
            return m + 1;

        }
        return getAckermannFunction(n, m);
    }
}
