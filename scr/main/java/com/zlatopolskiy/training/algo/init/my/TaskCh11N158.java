package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 11.158.*Удалить из массива все повторяющиеся элементы, оставив их первые вхо-
 * ждения, т. е. в массиве должны остаться только различные элементы.
 * */

import java.util.Scanner;

public class TaskCh11N158 {
    public static void main(String[] args) {
        String inputDataStr = inputStr();
        int[] dataArr = strToIntArray(inputDataStr);
        removeDoubleValue(dataArr);
        removeDoubleValue(dataArr);
        printArray(dataArr);
    }

    private static String inputStr() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a sequence of numbers separated by a space : ");
        String inputStr = sc.nextLine();

        return inputStr;
    }

    private static int[] strToIntArray(String str) {
        StringBuilder sbStr = new StringBuilder(str);
        String[] arrayStr = sbStr.toString().split("\\s+");

        int[] arrayNum = new int[arrayStr.length];
        for (int i = 0; i < arrayStr.length; i++) {
            arrayNum[i] = Integer.parseInt(arrayStr[i]);
        }
        return arrayNum;
    }

    private static void moveDoubleValueInTail(int number, int[] arr) {
        for (int i = number; i < arr.length - 1; i++) {
            int tmp = arr[i];
            arr[i] = arr[i + 1];
            arr[i + 1] = tmp;
        }
    }

    public static int[] removeDoubleValue(int[] arr) {
        for (int element = 0; element <= arr.length - 1; element++) {
            int countOfOriginalValue = 0;
            for (int simple = 0; simple <= arr.length - 1; simple++) {
                if (arr[element] == arr[simple]) {
                    countOfOriginalValue++;
                    if (countOfOriginalValue > 1) {
                        arr[simple] = 0;
                        moveDoubleValueInTail(simple, arr);
                    }
                }
            }
        }
        return arr;
    }

    private static void printArray(int[] arr) {
        for (int i = 0; i <= arr.length - 1; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
