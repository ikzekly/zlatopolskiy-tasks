package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 6.8. Дано число n. Из чисел 1, 4, 9, 16, 25, ... напечатать те, которые не превыша-
 * ют n.
 */

public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.println("Enter the limit for the output values:");
        int enterLimit = TaskCh01N003.inputNum();
        outputResult(compareNum(enterLimit));
    }

    public static int[] compareNum(int limit) {
        int lengthArrIsSqrtLimit = (int) Math.sqrt(limit);
        int[] numbers = new int[lengthArrIsSqrtLimit];
        for (int num = 1; num * num <= limit; num++) {
            numbers[num - 1] = num * num;
        }
        return numbers;
    }

    private static void outputResult(int[] num) {
        for (int numForOut : num) {
            System.out.print(numForOut + " ");
        }
    }
}
