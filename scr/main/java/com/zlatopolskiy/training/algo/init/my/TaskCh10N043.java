package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.43. Написать рекурсивную функцию:
 * а) вычисления суммы цифр натурального числа;
 * б) вычисления количества цифр натурального числа.
 * */

public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        int num = TaskCh01N003.inputNum();
        System.out.println("The number of digits in the number = " + countOfDigit(num) + " ;");
        System.out.println("The sum is the number of digits = " + sumOfDigitInNumber(num) + " ;");
    }

    public static int sumOfDigitInNumber(int num) {
        if (num / 10 != 0)
            return num % 10 + sumOfDigitInNumber(num / 10);
        else return num;
    }

    public static int countOfDigit(int num) {
        if (num / 10 != 0)
            return 1 + countOfDigit(num / 10);
        else
            return 1;
    }
}
