package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.56.* Написать рекурсивную функцию, определяющую, является ли заданное на-
 * туральное число простым (простым называется натуральное число, боль-
 * шее 1, не имеющее других делителей, кроме единицы и самого себя).
 * */

public class TaskCh10N056 {
    public static void main(String[] args) {
        System.out.println("Enter natural number: ");
        int num = TaskCh01N003.inputNum();
        System.out.println(checkNumForPrime(num, 2));
    }

    public static boolean checkNumForPrime(int num, int count) {
        if (num <= 2) {
            return true;
        }
        while (count < Math.sqrt(num)) {
            if (num % count == 0) {
                return false;
            } else {
                checkNumForPrime(num, ++count);
            }
        }
        return true;
    }
}