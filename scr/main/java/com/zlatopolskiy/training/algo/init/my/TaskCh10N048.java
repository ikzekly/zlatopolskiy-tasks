package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.48. Написать рекурсивную функцию для вычисления максимального элемента
 * массива из n элементов.
 * */

import java.util.Scanner;

public class TaskCh10N048{
    public static void main(String[] args) {
        String str = inputStr();
        int[] intArrayFromStr = strToIntArray(str);

        System.out.println(getMaxValueInArr(intArrayFromStr, 0));
    }

    private static String inputStr() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a sequence of numbers separated by a space : ");
        String inputStr = sc.nextLine();

        return inputStr;
    }

    private static int[] strToIntArray(String str) {
        StringBuilder sbStr = new StringBuilder(str);
        String[] arrayStr = sbStr.toString().split("\\s+");

        int[] arrayNum = new int[arrayStr.length];
        for (int i = 0; i < arrayStr.length; i++) {
            arrayNum[i] = Integer.parseInt(arrayStr[i]);
        }
        return arrayNum;
    }

    public static int getMaxValueInArr(int[] inputArr, int index) {
        int maxValue = inputArr[index];
        for (int i = 1; i < inputArr.length; i++) {
            if (maxValue < inputArr[i]) {
                maxValue = getMaxValueInArr(inputArr, i);
            }
        }
        return maxValue;
    }
}