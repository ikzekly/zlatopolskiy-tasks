package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 11.245.*Дан массив. Переписать его элементы в другой массив такого же размера
 * следующим образом: сначала должны идти все отрицательные элементы,
 * а затем все остальные. Использовать только один проход по исходному
 * массиву.
 * */

import java.util.Scanner;

public class TaskCh11N245 {
    public static void main(String[] args) {
        String inputedDataStr = inputStr();
        int[] dataArr = strToIntArray(inputedDataStr);
        int [] result = sortArray(dataArr);
        printArray(result);
    }

    private static String inputStr() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a sequence of numbers separated by a space : ");
        String inputStr = sc.nextLine();

        return inputStr;
    }

    private static int[] strToIntArray(String str) {
        StringBuilder sbStr = new StringBuilder(str);
        String[] arrayStr = sbStr.toString().split("\\s+");

        int[] arrayNum = new int[arrayStr.length];
        for (int i = 0; i < arrayStr.length; i++) {
            arrayNum[i] = Integer.parseInt(arrayStr[i]);
        }
        return arrayNum;
    }

    public static int[] sortArray(int[] arr) {
        int[] resultArr = new int[arr.length];
        int negativeNumCount = 0;
        int positiveNumCount = 0;
        for (int num : arr) {
            if (num < 0) {
                resultArr[negativeNumCount] = num;
                negativeNumCount++;
            } else {
                resultArr[arr.length - 1 - positiveNumCount] = num;
                positiveNumCount++;
            }
        }
        return resultArr;
    }

    private static void printArray(int[] arr) {
        for (int i = 0; i <= arr.length - 1; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
