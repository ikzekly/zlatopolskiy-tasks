package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.15. Дано слово. Вывести на экран его k-й символ.
 * */

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {

        String str = inputStr();
        int num = inputNumOfChar();
        checkNum(num, str);
        String foundChar = lookingChar(str, num);
        System.out.println(foundChar);
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the string: ");
        String str = scNumber.next();

        return str;
    }

    private static int inputNumOfChar() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the number of characters: ");
        int num = scNumber.nextInt();

        return num;
    }

    private static int checkNum(int num, String str) {
        int allowedLength = str.length() + 1;
        if (num >= allowedLength || num == 0) {
            throw new RuntimeException("Incorrect number. Your value should be less than " +
                    allowedLength + " and more than 0");
        }
        return num;
    }

    public static String lookingChar(String str, int num) {
        num = num - 1;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        Character foundChar = sb.charAt(num);

        return foundChar.toString();
    }
}
