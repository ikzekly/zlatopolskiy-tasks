package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.17. Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же
 * букву?
 * */

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {

        Boolean result = compareFirstAndLastChars(inputStr());
        System.out.println(result);
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the string: ");
        String str = scNumber.next();

        return str;
    }

    public static boolean compareFirstAndLastChars(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        String firstChar = sb.charAt(0) + "";
        String lastChar = sb.charAt(sb.length() - 1) + "";
        if (firstChar.equals(lastChar))
            return true;
        else
            return false;
    }
}
