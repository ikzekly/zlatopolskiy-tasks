package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

/**
 * Created by mr.robot on 4/20/2016.
 */
public class TaskCh02N043Test {
    public static void main(String[] args) {
testModulo();
    }
private static void testModulo(){
    Assert.assertEquals("TaskCh02N043", 1, TaskCh02N043.modulo(2,4));
    Assert.assertEquals("TaskCh02N043", 4, TaskCh02N043.modulo(3,4));
}
}
