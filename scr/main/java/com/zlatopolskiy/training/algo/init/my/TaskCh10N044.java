package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.44. Написать рекурсивную функцию нахождения цифрового корня натурального
 * числа. Цифровой корень данного числа получается следующим образом. Ес-
 * ли сложить все цифры этого числа, затем все цифры найденной суммы
 * и повторять этот процесс, то в результате будет получено однозначное число
 * (цифра), которая и называется цифровым корнем данного числа.
 * */

public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.println("Enter the number: ");
        int num = TaskCh01N003.inputNum();
        System.out.println("Digit root of number = " + getDigitRootOfNumber(num));

    }

    public static int getDigitRootOfNumber(int num) {
        if (num / 10 != 0)
            return getDigitRootOfNumber(num % 10 + getDigitRootOfNumber(num / 10));
        else return num;

    }
}