package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        testCheckSeason();
    }
    private static void testCheckSeason() {
        Assert.assertEquals("TaskCh04N106", "Winter", TaskCh04N106.checkSeason(12));
    }
}
