package main.java.com.zlatopolskiy.training.algo.init.my;
/**
 * 1.17. Записать по правилам изучаемого языка программирования следующие вы-
 ражения:
 * */
public class TaskCh01N017 {
    public static void main(String[] args) {
        double x = 1;
        double a = 1;
        double b = 1;
        double c = 1;

        double o = Math.sqrt(1 - Math.pow(Math.sin(x), 2));

        double p = 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);

        double r = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));

        double s = Math.abs(x) + Math.abs(x + 1);
    }
}
