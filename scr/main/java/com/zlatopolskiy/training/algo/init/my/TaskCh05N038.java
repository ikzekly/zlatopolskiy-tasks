package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 5.38. "Странный муж". Некий мужчина отправляется на работу, которая нахо-
 * дится на расстоянии 1 км от дома. Дойдя до места работы, он вдруг вспомина-
 * ет, что перед уходом забыл поцеловать жену, и поворачивает назад. Пройдя
 * полпути, он меняет решение, посчитав, что правильнее вернуться на работу.
 * Пройдя 1/3 км по направлению к работе, он вдруг осознает, что будет на-
 * стоящим подлецом, если так и не поцелует жену. На этот раз, прежде чем из-
 * менить мнение, он проходит 1/4 км. Так он продолжает метаться, и после N-
 * этапа, пройдя 1/N км, снова меняет решение.
 * Определить:
 * а) на каком расстоянии от дома будет находиться мужчина после 100-го этапа
 * (если допустить, что такое возможно);
 * б) какой общий путь он при этом пройдет.
 * */

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaskCh05N038 {
    public static void main(String[] args) {

        System.out.println("Distance from home after the 100-th stage = " + findDistToHome() + " km.");
        System.out.println("Full distance traveled = " + findFullDistance() + " km.");
    }

    private static boolean isEvenOrOdd(double num) {
        if (num % 2 == 0) {
            return true;
        } else
            return false;
    }

    public static double findDistToHome() {
        double sumEven = 0.0;
        double sumOdd = 0.0;
        for (double n = 1; n <= 100; n++) {
            if (isEvenOrOdd(n)) {
                sumEven += (1 / n);
            } else {
                sumOdd += -1 * (1 / n);
            }
        }
        return new BigDecimal((sumEven + sumOdd) * (-1)).setScale(2, RoundingMode.HALF_UP).doubleValue();

    }

    public static double findFullDistance() {
        double distResult = 0.0;
        for (double n = 1; n <= 100; n++) {
            distResult += (1 / n);
        }
        return new BigDecimal(distResult).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
