package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 9.166. Дано предложение. Поменять местами его первое и последнее слово.
 * */

import java.util.Scanner;

public class TaskCh09N166 {
    public static void main(String[] args) {

        String str = inputStr();
        String result = replaceWords(str);
        System.out.println(result);
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);
        System.out.println("Enter the string: ");
        return scNumber.nextLine();
    }

    private static String[] removePunctuation(String[] string) {
        for (int i = 0; i < string.length; i++) {
            string[i] = string[i].replaceAll("[^\\w]", "");
        }
        return string;
    }

    private static String arrayToStr(String[] arrToConvert) {
        StringBuilder sbStrResult = new StringBuilder();
        for (String pieceOfStr : arrToConvert) {
            sbStrResult = sbStrResult.append(pieceOfStr + " ");
        }
        return sbStrResult.toString();
    }

    public static String replaceWords(String strForChanges) {
        StringBuilder sbStrForChanges = new StringBuilder(strForChanges);
        String[] arrSplitLineForWords = sbStrForChanges.toString().split("\\s+");

        removePunctuation(arrSplitLineForWords);

        String tmp = arrSplitLineForWords[0];
        arrSplitLineForWords[0] = arrSplitLineForWords[arrSplitLineForWords.length - 1];
        arrSplitLineForWords[arrSplitLineForWords.length - 1] = tmp;

        String resultOfChangedStr = arrayToStr(arrSplitLineForWords);
        return resultOfChangedStr;
    }

}

