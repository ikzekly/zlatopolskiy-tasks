package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 2.13. Дано трехзначное число. Найти число, полученное при прочтении его цифр
 * справа налево.
 */

import java.util.InputMismatchException;

public class TaskCh02N013 {
    public static void main(String[] args) {
        try {
            System.out.println("Enter three-digit number: ");
            int num = TaskCh01N003.inputNum();

            if (checkTheThreeDigitNumber(num)) {
                int resultingValue = invertValue(num);
                System.out.println(resultingValue);
            }
        } catch (InputMismatchException e) {
            System.out.println("Input error! You must enter a number.");
            e.printStackTrace();
        }
    }

    public static boolean checkTheThreeDigitNumber(int num) throws InputMismatchException {
        if (num >= 100 && num <= 999) {
            return true;
        } else throw new InputMismatchException("No. You must enter three-digit number!");
    }

    public static int invertValue(int numberToConvert) {

        int countOfTen = 1;
        int resultInvert = 0;
        int copyNumberToConvert = numberToConvert;

        while (numberToConvert > 0) {

            while (copyNumberToConvert > 10) {
                countOfTen = countOfTen * 10;
                copyNumberToConvert /= 10;
            }

            int resultWhile = numberToConvert % 10;
            numberToConvert /= 10;
            resultWhile = resultWhile * countOfTen;
            countOfTen /= 10;

            resultInvert = resultInvert + resultWhile;
        }
        return resultInvert;
    }
}





