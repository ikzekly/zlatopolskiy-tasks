package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 4.115.*В некоторых странах Дальнего Востока (Китае, Японии и др.) использовался
 * (и неофициально используется в настоящее время) календарь, отличающийся
 * от применяемого нами. Этот календарь представляет собой 60-летнюю цик-
 * лическую систему. Каждый 60-летний цикл состоит из пяти 12-летних под-
 * циклов. В каждом подцикле года носят названия животных: Крыса, Корова,
 * Тигр, Заяц, Дракон, Змея, Лошадь, Овца, Обезьяна, Петух, Собака и Свинья.
 * Кроме того, в названии года фигурируют цвета животных, которые связаны
 * с пятью элементами природы — Деревом (зеленый), Огнем (красный), Зем-
 * лей (желтый), Металлом (белый) и Водой (черный). В результате каждое жи-
 * вотное (и его год) имеет символический цвет, причем цвет этот часто совер-
 * шенно не совпадает с его "естественной" окраской — Тигр может быть чер-
 * ным, Свинья — красной, а Лошадь — зеленой. Например, 1984 год — год
 * начала очередного цикла — назывался годом Зеленой Крысы. Каждый цвет
 * в цикле (начиная с зеленого) "действует" два года, поэтому через каждые
 * 60 лет имя года (животное и его цвет) повторяется.
 * Составить программу, которая по заданному номеру года нашей эры n печа-
 * тает его название по описанному календарю в виде: "Крыса, Зеленый". Рас-
 * смотреть два случая:
 * а) значение n 1984;
 * б) значение n может быть любым натуральным числом.
 */
public class TaskCh04N115 {
    public static void main(String[] args) {
        try {
            System.out.println("Input year: ");
            int year = TaskCh01N003.inputNum();
            checkForNaturalNum(year);
            System.out.println(conversionYear(year));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private static int checkForNaturalNum(int year) throws RuntimeException {
        if (year > 0)
            return year;
        else
            throw new RuntimeException("Input error! Your must enter year > 0.");
    }

    /**
     * Since the first year of the sixty-year cycle A.D. = 4,
     * and we need to start counting from 0,
     * I shifted the calculus on 3 years ago.
     * It turns the cycle of animals begins not with rat but with chicken.
     */
    public static String conversionYear(int gregorianYear) {

        int placeSixtyYearCycle = gregorianYear % 60;
        int placeTwelfthYearCycle = gregorianYear % 12;
        int placeColorCycle = placeSixtyYearCycle % 10;

        return checkAnimalColor(placeColorCycle) + " " + checkAnimal(placeTwelfthYearCycle);

    }

    private static String checkAnimal(int modulo) {
        String animal = "";
        switch (modulo) {
            case 1:
                animal = "Chicken";
                break;
            case 2:
                animal = "Dog";
                break;
            case 3:
                animal = "Pig";
                break;
            case 4:
                animal = "Rat";
                break;
            case 5:
                animal = "Cow";
                break;
            case 6:
                animal = "Tiger";
                break;
            case 7:
                animal = "Rabbit";
                break;
            case 8:
                animal = "Dragon";
                break;
            case 9:
                animal = "Snake";
                break;
            case 10:
                animal = "Horse";
                break;
            case 11:
                animal = "Sheep";
                break;
            case 12:
                animal = "Monkey";
                break;
        }
        return animal;
    }

    private static String checkAnimalColor(int modulo) {
        String color = "";
        switch (modulo) {
            case 4:
            case 5:
                color = "Green";
                break;
            case 6:
            case 7:
                color = "Red";
                break;
            case 8:
            case 9:
                color = "Yellow";
                break;
            case 10:
            case 1:
                color = "White";
                break;
            case 2:
            case 3:
                color = "Black";
                break;
        }
        return color;
    }
}
