package main.java.com.zlatopolskiy.training.algo.init.my;
/**
 * 10.53. Написать рекурсивную процедуру для ввода с клавиатуры последовательно-
 * сти чисел и вывода ее на экран в обратном порядке (окончание последова-
 * тельности — при вводе нуля).
 * */

import java.util.Scanner;

public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] array = inputSeriesOfNum();
        invertArr(array, 0);
    }

    private static int[] inputSeriesOfNum() {
        Scanner scNum = new Scanner(System.in);
        int num = -1;
        int[] inputArr = new int[100];
        int i = 0;
        while (num != 0) {
            num = scNum.nextInt();
            inputArr[i] = num;
            i++;
        }
        int[] result = new int[i];
        System.arraycopy(inputArr, 0, result, 0, i);
        return result;
    }

    public static void invertArr(int[] array, int i) {

        if (i < array.length - 1) {
            i++;
            invertArr(array, i);
            int currentNumber = i - 1;
            System.out.print(array[currentNumber]);
        }
    }
}
