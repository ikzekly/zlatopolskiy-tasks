package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.24. Заполнить массив размером 6*6 так, как показано на рис. 12.2.
 * */

public class TaskCh12N024 {
    public static void main(String[] args) {
        int[][] arrA = new int[6][6];
        System.out.println("Matrix A: ");
        printMatrix(matrixDiagonal(arrA));

        int[][] arrB = new int[6][6];
        System.out.println("Matrix B: ");
        printMatrix(matrix(arrB));
    }

    public static int[][] matrixDiagonal(int[][] arr) {
        int size = arr.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0 || j == 0) {
                    arr[i][j] = 1;
                } else {
                    arr[i][j] = arr[i - 1][j] + arr[i][j - 1];
                }
            }
        }
        return arr;
    }

    public static int[][] matrix(int[][] arr) {
        int size = arr.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                arr[i][j] = i + j + 1;
                if (arr[i][j] > size) {
                    arr[i][j] -= size;
                }
            }
        }
        return arr;
    }

    private static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
