package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.234. Дан двумерный массив.
 * а) Удалить из него k-ю строку.
 * б) Удалить из него s-й столбец.
 * */

public class TaskCh12N234 {
    public static void main(String[] args) {

        System.out.println("Input matrix size: ");
        System.out.print("length = ");
        int length = TaskCh01N003.inputNum();
        System.out.print("width = ");
        int width = TaskCh01N003.inputNum();
        System.out.println();

        int[][] matrix = inputMatrix(length, width);
        printMatrix(matrix);
        System.out.println();

        System.out.println("If you want delete string, input '1', if you want delete column input '2'.. ");
        int menu = TaskCh01N003.inputNum();
        checkInputValueInMenu(menu);

        switch (menu) {
            case 1:
                System.out.println("Input num of str for del: ");
                int numStrDel = TaskCh01N003.inputNum();
                if (numStrDel > width || numStrDel < 1) {
                    throw new RuntimeException("Invalid value," +
                            " number of string to delete can't be more than amount = " + width);
                } else
                    delStrInMatrix(matrix, numStrDel);
                break;
            case 2:
                System.out.println("Input num of column for del: ");
                int numColumnDel = TaskCh01N003.inputNum();
                if (numColumnDel > length || numColumnDel < 1) {
                    throw new RuntimeException("Invalid value," +
                            " number of column to delete can't be more than amount = " + length);
                } else
                    delColumnInMatrix(matrix, numColumnDel);
                break;
        }
        printMatrix(matrix);
    }

    private static int[][] inputMatrix(int lengthArr, int widthArr) {
        int[][] matrix = new int[lengthArr][widthArr];

        for (int i = 0; i < lengthArr; i++) {
            for (int j = 0; j < widthArr; j++) {
                System.out.print("[" + i + "][" + j + "]= ");
                matrix[i][j] = TaskCh01N003.inputNum();
            }
            System.out.println();
        }
        return matrix;
    }

    private static void checkInputValueInMenu(int num) throws RuntimeException {
        if (num > 2 || num < 1) {
            throw new RuntimeException("Invalid input value.");
        }
    }

    public static int[][] delStrInMatrix(int[][] matrix, int num) {
        int numOfStrForDell = num - 1;
        for (int i = numOfStrForDell; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i + 1 == matrix.length) {
                    matrix[i][j] = 0;
                } else
                    matrix[i][j] = matrix[i + 1][j];
            }
        }
        return matrix;
    }

    public static int[][] delColumnInMatrix(int[][] matrix, int num) {
        int numOfColumnForDell = num - 1;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = numOfColumnForDell; j < matrix[i].length; j++) {
                if (j + 1 == matrix[i].length) {
                    matrix[i][j] = 0;
                } else
                    matrix[i][j] = matrix[i][j + 1];
            }
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

}
