package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 2.39. Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
 * момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
 * между положением часовой стрелки в начале суток и в указанный момент
 * времени.
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        Time inputTimeObject = inputTime();
        try {
            int hour = inputTimeObject.getHour();
            checkingDigitOfHours(hour);
            int min = inputTimeObject.getMin();
            checkingDigitOfMin(min);
            int sec = inputTimeObject.getSec();
            checkingDigitOfSec(sec);
            int result = angleCalculation(hour, min, sec);
            System.out.println(result);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input!");
            e.printStackTrace();
        }
    }

    private static boolean checkingDigitOfHours(int hour) throws InputMismatchException {
        if (hour < 24 && hour >= 0) {
            return true;
        } else throw new InputMismatchException();
    }

    private static boolean checkingDigitOfMin(int min) throws InputMismatchException {
        if (min < 60 && min >= 0) {
            return true;
        } else throw new InputMismatchException();
    }

    private static boolean checkingDigitOfSec(int sec) throws InputMismatchException {
        if (sec < 60 && sec >= 0) {
            return true;
        } else throw new InputMismatchException();
    }

    private static Time inputTime() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the number of hours: ");
        int hour = scNumber.nextInt();

        System.out.println("Enter the number of minutes: ");
        int min = scNumber.nextInt();

        System.out.println("Enter the number of seconds: ");
        int sec = scNumber.nextInt();

        Time inputTime = new Time(hour, min, sec);

        inputTime.setHour(hour);
        inputTime.setMin(min);
        inputTime.setSec(sec);

        return inputTime;
    }

    public static int angleCalculation(int hour, int min, int sec) {

        int hourInSec;

        if (hour > 12) {
            hourInSec = (hour - 12) * 60 * 60;
        } else hourInSec = hour * 60 * 60;

        int minInSec = min * 60;

        int resultAngle = (hourInSec + minInSec + sec) / 120;

        return resultAngle;
    }
}

class Time {
    private int hour;
    private int min;
    private int sec;

    Time(int hour, int min, int sec) {
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getSec() {
        return sec;
    }


    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }
}
