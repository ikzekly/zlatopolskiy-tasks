package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.23. Заполнить двумерный массив размером 7*7 так, как показано на рис. 12.1.
 * */

public class TaskCh12N023 {
    public static void main(String[] args) {
        int[][] arrA = new int[7][7];
        System.out.println("Matrix A - Diagonal: ");
        printMatrix(matrixDiagonal(arrA));

        int[][] arrB = new int[7][7];
        System.out.println("Matrix B - Star:");
        printMatrix(matrixStar(arrB));

        int[][] arrC = new int[7][7];
        System.out.println("Matrix C - Sandglass:");
        printMatrix(matrixSandglass(arrC));

    }

    public static int[][] matrixDiagonal(int[][] arr) {
        int size = arr.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if ((j == size - 1 - i) || (j == i)) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;

    }

    public static int[][] matrixStar(int[][] arr) {
        int size = arr.length;
        int center = size / 2;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if ((j == size - 1 - i) || (j == i) || i == center || j == center) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] matrixSandglass(int[][] arr) {
        int size = arr.length;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if ((j == size - 1 - i) || (j == i) ) {
                    arr[i][j] = 1;
                } else if ((j < i) && (j > size - 1 - i) || (j > i) && (j < size - 1 - i)){
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    private static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}

