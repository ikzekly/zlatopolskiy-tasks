package main.java.com.zlatopolskiy.training.algo.init.my;

/**9.107. Дано слово. Поменять местами первую из букв а и последнюю из букв о.
 * Учесть возможность того, что таких букв в слове может не быть.
 */

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        String str = inputStr();
        System.out.println(replaceChars(str));
    }

    private static String inputStr() {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the string: ");
        String str = scNumber.next();

        return str;
    }

    private static String replaceChars(String str) {
        String[] lines = str.split("");
        StringBuilder sbResult = new StringBuilder(str);

        if (str.contains("a") || str.contains("o")) {
            int posOfA = 0;
            int posOfO = 0;
            for (String character : lines) {
                posOfA = str.indexOf('a');
                posOfO = str.lastIndexOf('o');
            }
            sbResult.setCharAt(posOfA, 'o');
            sbResult.setCharAt(posOfO, 'a');
        }
        return sbResult.toString();
    }
}
