package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.51. Определить результат выполнения следующих рекурсивных процедур при
 * n=5 :
 * */

public class TaskCh10N051 {
    public static void main(String[] args) {
        System.out.println("Enter n: ");
        int n = TaskCh01N003.inputNum();
        procedure1(n);
        System.out.println("\n");
        procedure2(n);
        System.out.println("\n");
        procedure3(n);
    }


    public static void procedure1(int n) {
        if (n > 0) {
            System.out.print(n + " ");
            procedure1(n - 1);
        }
    }

    public static void procedure2(int n) {
        if (n > 0) {
            procedure2(n - 1);
            System.out.print(n + " ");
        }
    }

    public static void procedure3(int n) {
        if (n > 0) {
            System.out.print(n + " ");
            procedure3(n - 1);
            System.out.print(n + " ");
        }
    }


}
