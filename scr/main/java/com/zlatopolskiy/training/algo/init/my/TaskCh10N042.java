package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 10.42. В некоторых языках программирования (например, в Паскале) не преду-
 * смотрена операция возведения в степень. Написать рекурсивную функцию
 * для расчета степени n вещественного числа a (n — натуральное число).
 * */

public class TaskCh10N042 {
    public static void main(String[] args) {

        System.out.println("Input number: ");
        int num = TaskCh01N003.inputNum();
        System.out.println("Input degree: ");
        int exp = TaskCh01N003.inputNum();
        System.out.println(getExpOfNum(num, exp));
    }

    public static int getExpOfNum(int num, int exp) {
        if (exp == 0)
            return 1;
        else
            return num * getExpOfNum(num, exp - 1);
    }
}