package main.java.com.zlatopolskiy.training.algo.init.my;

import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testFinAge();
    }
    private static void testFinAge(){
        Assert.assertEquals("TaskCh04N015",29,TaskCh04N015.findAge(6,1985,12,2014));
        Assert.assertEquals("TaskCh04N015",28,TaskCh04N015.findAge(6,1985,5,2014));
        Assert.assertEquals("TaskCh04N015",29,TaskCh04N015.findAge(6,1985,6,2014));
    }
}
