package main.java.com.zlatopolskiy.training.algo.init.my;


import main.java.com.zlatopolskiy.training.algo.init.Assert;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testCheckColorLight();
    }

    private static void testCheckColorLight() {
        Assert.assertEquals("TaskCh04N036", "red", TaskCh04N036.checkColorLight(3));
        Assert.assertEquals("TaskCh04N036", "green", TaskCh04N036.checkColorLight(5));
    }
}
