package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 5.64. В области 12 районов. Известны количество жителей (в тысячах человек)
 * и площадь (в км2) каждого района. Определить среднюю плотность населения
 * по области в целом.
 * <p>
 * use array as arg in solution method
 */

public class TaskCh05N064 {
    public static void main(String[] args) {
        int[] data = new int[12];
        for (int i = 0; i < 12; i++) {
            int number = i + 1;
            System.out.println("Area - " + number);
            data[i] = apdInTheArea();
        }
        int result = apdResult(data);
        System.out.println("The average population density in the area is " + result);
    }

    private static int apdInTheArea() {
        System.out.println("Enter the count of people in area.");
        int peopleCount = TaskCh01N003.inputNum();
        System.out.println("Enter the square of area.");
        int square = TaskCh01N003.inputNum();

        return square / peopleCount;
    }

    public static int apdResult(int[] data) {
        int apdAll = 0;
        for (int apdArea : data) {
            apdAll += apdArea;
        }
        int res = apdAll / 12;
        return res;
    }
}
