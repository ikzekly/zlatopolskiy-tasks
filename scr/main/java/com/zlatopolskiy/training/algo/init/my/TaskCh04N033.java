package main.java.com.zlatopolskiy.training.algo.init.my;

import java.util.InputMismatchException;

/**
 * 4.33. Дано натуральное число.
 * а) Верно ли, что оно заканчивается четной цифрой?
 * б) Верно ли, что оно заканчивается нечетной цифрой?
 */

public class TaskCh04N033 {
    public static void main(String[] args) {
      try {
          System.out.println("Enter the num:");
          int num = TaskCh01N003.inputNum();
          boolean result = evenOrOddNum(num);
          outputRes(result);
      }catch (InputMismatchException e){
          e.printStackTrace();
      }
    }

    public static boolean evenOrOddNum(int num) {
        int lastNum = num % 10;

        return lastNum % 2 == 0 ? true : false;

    }

    private static void outputRes(boolean evenOrOdd) {
        if (evenOrOdd) {
            System.out.println("Your number ends with an EVEN number");
        } else
            System.out.println("Your number ends with an ODD number.");
    }
}
