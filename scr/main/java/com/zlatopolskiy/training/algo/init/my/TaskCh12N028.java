package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 12.28.*Заполнить двумерный массив размером 5*5 так, как представлено на
 * рис. 12.4.
 *
 * (solve it for array n*n, where n%2=1)
 * */

import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        int dim = inputMatrixDim();
        int[][] arr = getMatrix(dim);

        printMatrix(arr);

    }

    private static int inputMatrixDim() throws RuntimeException {
        Scanner scNumber = new Scanner(System.in);

        System.out.println("Enter the matrix dimension (an odd number):");
        int inputDim = 0;
        try {
            inputDim = scNumber.nextInt();
            if (inputDim % 2 == 0) {
                throw new RuntimeException("The entered number must be odd dimension.");
            }
        } catch (RuntimeException e) {
            throw e;
        }
        return inputDim;
    }

    public static int[][] getMatrix(int length) {
        int weight = length;
        int[][] arr = new int[length][weight];
        int roundNum = 0;
        int numOfRoundsInTheMatrix = length / 2 + length % 2;

        while (roundNum <= numOfRoundsInTheMatrix) {
            moveAround(arr, roundNum);
            roundNum++;
        }
        return arr;
    }

    /**
     * roundNum - count of shells.
     */
    private static int[][] moveAround(int[][] arr, int roundNum) {
        int length = arr.length - 1;
        int num = 0;
/**
 * Check the value of element for start.
 *
 * */
        if (roundNum > 0) {
            for (int i = roundNum; i > 0; i--) {
                num = num + (length + 1 - roundNum) * 4;
            }
        } else {
            num = 0;
        }
/**
 * If (last element) ->
 * We calculate it and put in the center.
 *
 * */
        if (roundNum > length - roundNum) {
            int center = (length + 1) * (length + 1);
            arr[length / 2][length / 2] = center;
        } else {
            /**
             * One loop = one side.
             * We are moving from the first element to the penultimate.
             * With each round it is compressed on roundNum.
             *
             * */
            for (int topSide = roundNum; topSide < length - roundNum; topSide++) {
                num++;
                arr[roundNum][topSide] = num;
            }
            for (int rightSide = roundNum; rightSide < length - roundNum; rightSide++) {
                num++;
                arr[rightSide][length - roundNum] = num;
            }
            for (int bottomSide = length - roundNum; bottomSide > roundNum; bottomSide--) {
                num++;
                arr[length - roundNum][bottomSide] = num;
            }
            for (int leftSide = length - roundNum; leftSide > roundNum; leftSide--) {
                num++;
                arr[leftSide][roundNum] = num;
            }
        }
        return arr;
    }

    private static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
