package main.java.com.zlatopolskiy.training.algo.init.my;
/**
 * 5.10. Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему
 * курсу (значение курса вводится с клавиатуры).
 */

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        try {
            System.out.println("Input exchange rate: ");
            double countRubToDollar = inputExchRate();
            if (countRubToDollar < 0) {
                throw new InputMismatchException();
            } else {
                printResult(countRubToDollar);
            }
        } catch (InputMismatchException e) {
            System.out.println("Wrong input!");
            e.printStackTrace();
        }
    }

    private static double inputExchRate() {
        Scanner scInputNum = new Scanner(System.in);
        double inputExchRateObj = scInputNum.nextDouble();

        return inputExchRateObj;
    }

    public static double findValueOfRubles(double exchangeRate, int countOfDollar) {
        double resultConverting = countOfDollar * exchangeRate;

        return new BigDecimal(resultConverting).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    private static void printResult(double exchangeRate) {
        for (int countOfDollar = 1; countOfDollar <= 20; countOfDollar++) {
            System.out.println(countOfDollar + " USD = " + findValueOfRubles(exchangeRate, countOfDollar) + " RUR");
        }
    }
}
