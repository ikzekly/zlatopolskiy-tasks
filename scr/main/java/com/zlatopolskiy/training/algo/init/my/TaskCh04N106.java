package main.java.com.zlatopolskiy.training.algo.init.my;

/**
 * 4.106. Составить программу, которая в зависимости от порядкового номера дня ме-
 * сяца (1, 2, ..., 12) выводит на экран время года, к которому относится этот
 * месяц.
 */

import java.util.InputMismatchException;

public class TaskCh04N106 {
    public static void main(String[] args) {
        try {
            int month = TaskCh01N003.inputNum();
            if (month >= 1 && month <= 12) {
                String season = checkSeason(month);
                System.out.println(season);
            } else throw new InputMismatchException();
        } catch (InputMismatchException e) {
            System.out.println("Wrong input!");
            e.printStackTrace();
        }
    }

    public static String checkSeason(int month) {
        String season = "";
        switch (month) {
            case 12:
            case 1:
            case 2:
                season = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                season = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                season = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                season = "Autumn";
                break;
        }
        return season;
    }
}
